package com.guruofjava.sudaksha.j45;

public class Animal {

	public void move(int distance) {

		System.out.println("Animal is moving " + distance + " distance.");
	}

	public static void whoAmI() {
		System.out.println("Animal");
	}

}
