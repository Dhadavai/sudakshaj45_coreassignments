package com.guruofjava.sudaksha.j45;

public interface Jumper extends Runner {
	
	void jump();

}