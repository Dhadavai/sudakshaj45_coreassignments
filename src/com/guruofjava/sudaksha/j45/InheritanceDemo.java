package com.guruofjava.sudaksha.j45;

public class InheritanceDemo {
	
	public static void main(String[] args) {
		Animal a1 = new Animal();
		a1.move(6);
		
		Cat c1 = new Cat();
		c1.move(20);
		
		Bird b1 = new Bird();
		b1.move(45);
		
		Animal a2 = new Cat();
		
		a1.whoAmI();
		a2.whoAmI();
		c1.whoAmI();
		a2.move(50);
		
		a2.move(500);
		//a2.hunt(); // While compilation it looks for Type.
		
		Animal2 a3 = new Fish();
		a3.move(25);
		
		
		c1.jump();
		c1.run();
	}

}
