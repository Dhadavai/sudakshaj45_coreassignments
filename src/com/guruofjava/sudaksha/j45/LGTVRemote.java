package com.guruofjava.sudaksha.j45;

public class LGTVRemote implements TVRemote{

	LGTV tv = new LGTV();
	
	@Override
	public void increaseVolumn() {
		tv.increaseVolume();
		
	}

	@Override
	public void decreaseVolumn() {
		tv.decreaseVolume();
		
	}

	@Override
	public void nextChannel() {
		tv.nextChannel();
		
	}

	@Override
	public void previousChannel() {
		tv.previousChannel();
		
	}

	@Override
	public void last() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void changeChannel() {
		tv.setChannel(125);
		
	}

	@Override
	public void powerOnOff() {
		tv.powerOnOff();
		
	}

	@Override
	public void mute() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void menu() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void guide() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void increaseBrightness() {
		tv.increaseBrightness();
		
	}

	@Override
	public void decreaseBrightness() {
		tv.decreaseBrightness();
		
	}

	@Override
	public void increaseContrast() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void decreaseContrast() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void increaseColor() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void decreaseColor() {
		// TODO Auto-generated method stub
		
	}
	
	public void startRecord(){
		
	}
	
	public void pause(){
		
	}
	
	public void stopRecord(){
		
	}
	
	
	

}
