package com.guruofjava.sudaksha.j45;

public class FanInvalidPriceException extends FanException {
	
	public FanInvalidPriceException(){
		super();
	}
	
	public  FanInvalidPriceException(String s){
		super(s);
	}
	
	public FanInvalidPriceException(Throwable t){
		super(t);
	}
	
	public FanInvalidPriceException(String s , Throwable t){
		super(s, t);
	}
	
}
