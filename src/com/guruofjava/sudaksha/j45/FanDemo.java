package com.guruofjava.sudaksha.j45;

public class FanDemo {

	public static void main(String[] args) {

		Fan f1 = new Fan();
		System.out.println("Fan Brand: " + f1.getBrand());
		System.out.println("Fan Color: " + f1.getColor());
		System.out.println("Number of Wings: " + f1.getNumberOfWings());
		System.out.println("Fan Price: " + f1.getPrice());
		System.out.println("Fan State: " + f1.getState());
		System.out.println("Fan current Speed: " + f1.getCurrentSpeed());
		f1.increaseSpeed();
		f1.increaseSpeed();
		System.out.println("Fan current Speed: " + f1.getCurrentSpeed());
		f1.decreaseSpeed();
		System.out.println("Fan current Speed: " + f1.getCurrentSpeed());
		
		f1.setPrice(1250);
		f1.switchOff();
		System.out.println("Fan Price: " + f1.getPrice());
		System.out.println("Fan State: " + f1.getState());
		f1.switchOn();
		System.out.println("Fan State: " + f1.getState());
		System.out.println("~~~~~~~~~~~~~~~~~~~~\n~~~~~~~~~~~~~~~~~~~~");
		
		Fan f2 = new Fan("Usha");
		System.out.println("Fan Brand: " + f2.getBrand());
		System.out.println("Fan Color: " + f2.getColor());
		System.out.println("Number of Wings: " + f2.getNumberOfWings());
		System.out.println("Fan Price: " + f2.getPrice());
		System.out.println("Fan State: " + f2.getState());
		f2.setPrice(5000);
		f2.switchOff();
		System.out.println("Fan Price: " + f2.getPrice());
		System.out.println("Fan State: " + f2.getState());
		f2.switchOn();
		System.out.println("Fan State: " + f2.getState());
		System.out.println("Fan current Speed: " + f2.getCurrentSpeed());
		f2.increaseSpeed();
		f2.increaseSpeed();
		System.out.println("Fan current Speed: " + f2.getCurrentSpeed());
		f2.decreaseSpeed();
		System.out.println("Fan current Speed: " + f2.getCurrentSpeed());
		System.out.println("~~~~~~~~~~~~~~~~~~~~\n~~~~~~~~~~~~~~~~~~~~");
		
		Fan f3 = new Fan("Crampton", 4);
		System.out.println("Fan Brand: " + f3.getBrand());
		System.out.println("Fan Color: " + f3.getColor());
		System.out.println("Number of Wings: " + f3.getNumberOfWings());
		System.out.println("Fan Price: " + f3.getPrice());
		System.out.println("Fan State: " + f3.getState());
		f3.setPrice(6000);
		f3.switchOff();
		System.out.println("Fan Price: " + f3.getPrice());
		System.out.println("Fan State: " + f3.getState());
		f3.switchOn();
		System.out.println("Fan State: " + f3.getState());
		System.out.println("Fan current Speed: " + f3.getCurrentSpeed());
		f3.increaseSpeed();
		f3.increaseSpeed();
		System.out.println("Fan current Speed: " + f3.getCurrentSpeed());
		f3.decreaseSpeed();
		System.out.println("Fan current Speed: " + f3.getCurrentSpeed());
		System.out.println("~~~~~~~~~~~~~~~~~~~~\n~~~~~~~~~~~~~~~~~~~~");
		
		Fan f4 = new Fan("Hawells", 6, "White");
		System.out.println("Fan Brand: " + f4.getBrand());
		System.out.println("Fan Color: " + f4.getColor());
		System.out.println("Number of Wings: " + f4.getNumberOfWings());
		System.out.println("Fan Price: " + f4.getPrice());
		System.out.println("Fan State: " + f4.getState());
		f4.setPrice(7500);
		f4.switchOff();
		System.out.println("Fan Price: " + f4.getPrice());
		System.out.println("Fan State: " + f4.getState());
		f4.switchOn();
		System.out.println("Fan State: " + f4.getState());
		System.out.println("Fan current Speed: " + f4.getCurrentSpeed());
		f4.increaseSpeed();
		f4.increaseSpeed();
		System.out.println("Fan current Speed: " + f4.getCurrentSpeed());
		f4.decreaseSpeed();
		System.out.println("Fan current Speed: " + f4.getCurrentSpeed());
		System.out.println("~~~~~~~~~~~~~~~~~~~~\n~~~~~~~~~~~~~~~~~~~~");
		
		Fan f5 = null;
		try {
			f5 = new Fan("Usha", 4, "Cream", 6900.99);
			System.out.println(f5.getPrice());
			f5.setPrice(6220.56);
			System.out.println(f5.getPrice());
	
		} catch(FanInvalidPriceException ifpe) {
			System.out.println("Invalid Price ");
			
		}
	
		
		Fan f6 = new Fan();
		System.out.println(f6.getPrice());
		try {
			f6.setPrice(-6000);	
		}catch(FanInvalidPriceException ifpe){
			System.out.println("You have entered invalid price. Please correct it!");
			//ifpe.printStackTrace();
		}
		
		System.out.println(f6.getPrice());
	}

}
