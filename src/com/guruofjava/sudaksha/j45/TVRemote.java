package com.guruofjava.sudaksha.j45;

public interface TVRemote {
	
	void increaseVolumn();
	
	void decreaseVolumn();
	
	void nextChannel();
	
	void previousChannel();
	
	void last();
	
	void changeChannel();
	
	void powerOnOff();
	
	void mute();
	
	void menu();
	
	void guide();
	
	void increaseBrightness();
	
	void decreaseBrightness();
	
	void increaseContrast();
	
	void decreaseContrast();
	
	void increaseColor();
	
	void decreaseColor();

}
