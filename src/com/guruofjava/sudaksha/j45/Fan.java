package com.guruofjava.sudaksha.j45;

public class Fan {
	
	private final String BRAND;
	private final int NUMBER_OF_WINGS;
	private final String COLOR;
	private double price;
	private String state="On";
	private int currentSpeed=0;
	
	public Fan(){
		BRAND="Bajaj";
		NUMBER_OF_WINGS=3;
		COLOR="White";
		price=1500.45;
	}
	
	public Fan(String brand){
		BRAND=brand;
		NUMBER_OF_WINGS=3;
		COLOR="Gray";
		price=2500.99;
	}
	
	public Fan(String brand, int wings){
		BRAND=brand;
		NUMBER_OF_WINGS=wings;
		COLOR="Cream";
		price=2300.99;
	}
	
	public Fan(String brand, int wings, String color){
		BRAND=brand;
		NUMBER_OF_WINGS=wings;
		COLOR=color;
		price=1900.85;
	}
	
	public Fan(String brand, int wings, String color, double price){
		BRAND=brand;
		NUMBER_OF_WINGS=wings;
		COLOR=color;
		if(price > 0){
			this.price=price;
		}
		else {
			//throw new FanException("for price:" + price);
			throw new FanInvalidPriceException("For Price" + price);
		}
		
	}
	
	public void switchOn(){
		if(state!="On"){
			state="On";
		}
	}
	public void switchOff(){
		if(state!="Off"){
			state="Off";
		}
	}
	public void setPrice(double price){
		if(price > 0){
			this.price=price;
		} else {
			//throw new IllegalArgumentException();
			throw new FanInvalidPriceException("For Price: " + price);
		}
		
	}
	public double getPrice(){
		return price;
	}
	
	public String getState(){
		return state;
	}
	
	public String getBrand(){
		return BRAND;
	}
	
	public String getColor(){
		return COLOR;
	}
	public int getNumberOfWings(){
		return NUMBER_OF_WINGS;
	}
	public void increaseSpeed(){
		if(currentSpeed==0){
			currentSpeed=1;
		}
		else if(currentSpeed==1){
			currentSpeed=2;
		}
		else if(currentSpeed==2){
			currentSpeed=3;
		}
		else if(currentSpeed==3){
			currentSpeed=4;
		}
		else if(currentSpeed==4){
			currentSpeed=5;
		}
		else if(currentSpeed==5){
			currentSpeed=0;
		}
	}
	public void decreaseSpeed(){
		if(currentSpeed==0){
			currentSpeed=5;
		}
		else if(currentSpeed==1){
			currentSpeed=0;
		}
		else if(currentSpeed==2){
			currentSpeed=1;
		}
		else if(currentSpeed==3){
			currentSpeed=2;
		}
		else if(currentSpeed==4){
			currentSpeed=3;
		}
		else if(currentSpeed==5){
			currentSpeed=4;
		}
	}
	public int getCurrentSpeed(){
		return currentSpeed;
	}

}
