package com.guruofjava.sudaksha.j45;

public class FanException extends RuntimeException {
	
	public FanException(){
		super();
	}
	
	public FanException(String s){
		super(s);
	}
	
	public FanException(Throwable t){
		super(t);
	}
	
	public FanException(String s, Throwable t){
		super(s, t);
	}

}
