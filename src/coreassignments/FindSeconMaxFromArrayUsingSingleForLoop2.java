package coreassignments;

public class FindSeconMaxFromArrayUsingSingleForLoop2 {
	public static void main(String[] args) {
		int a[] = {12, 36, 93, 49, 62, 9, 18, 67, 99};
		int max = a[0];
		for(int i = 1; i < 9; i++){
			if(max < a[i]){
				max = a[i];
			}
		}
	}
}
