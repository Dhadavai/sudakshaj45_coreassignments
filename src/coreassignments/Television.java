package coreassignments;

public class Television {
	
	public static String CATEGORY = "Electronics";
	//public static final String CATEGORY = "Electronics";
	private final byte MAX_VOLUM = 100;
	private final int MAX_CHANNEL = 99999;
	private final byte MAX_BRIGHTNESS = 100;
	public final String BRAND_NAME;
	public final double SIZE;
	private double price;
	public String state = "Off";
	private byte brightness = 50;
	private byte volume;
	private int channelNumber;
	
	public static void setCategory(String CATEGORY){
		//this.CATEGORY = CATEGORY; // this keyword cannot be used in the static method.
		Television.CATEGORY = CATEGORY;
	}

	public Television() {
		BRAND_NAME = "SONY";
		SIZE = 32.10;
		price = 39500;
	}

	public Television(String BRAND_NAME) {
		this.BRAND_NAME = BRAND_NAME;
		SIZE = 56.10;
		price = 59999.99;

	}

	public Television(String BRAND_NAME, double SIZE) {
		this.BRAND_NAME = BRAND_NAME;
		this.SIZE = SIZE;
		price = 2900.59;
	}

	public Television(String BRAND_NAME, double SIZE, double price) {
		this.BRAND_NAME = BRAND_NAME;
		this.SIZE = SIZE;
		this.price = price;
	}
	
	

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		if(price>=4000){
			this.price = price;
		}
		else {
			System.out.println("Invalid Price");
		}
		
	}

	public void powerOnOff() {
		if (state == "Off") {
			state = "On";
		} else {
			state = "Off";
		}
	}

	public String getState() {
		return state;
	}

	public void setChannel(int channelNumber) {
		if (channelNumber > 0 && channelNumber < MAX_CHANNEL) {

			this.channelNumber = channelNumber;
		} else {
			System.out.println("Please Enter valid number");
		}

	}

	public int getCurrentChannel() {
		return channelNumber;
	}

	public void increaseVolume() {
		if (volume < MAX_VOLUM)
			volume++;
	}

	public void decreaseVolume() {
		if (volume > 0) 
			volume--;
	}

	public void nextChannel() {
		if (channelNumber < MAX_CHANNEL)
			channelNumber++;
	}

	public void previousChannel() {
		if (channelNumber > 0) {
			channelNumber--;
		} else {
			System.out.println("Reached Min Channel");
		}

	}

	public byte getCurrentVolume() {
		return volume;
	}

	public void increaseBrightness() {
		if (brightness > MAX_BRIGHTNESS) {
			brightness++;
		}
	}

	public void decreaseBrightness() {
		if (brightness < 0) {
			brightness--;
		}
	}

	public byte getCurrentBrightness() {
		return brightness;
	}
	
	public void mute(){
		volume=0;
		
	}

}
