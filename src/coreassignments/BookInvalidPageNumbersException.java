package coreassignments;

public class BookInvalidPageNumbersException extends BookException {
	
	public BookInvalidPageNumbersException(){
		super();
	}
	
	public BookInvalidPageNumbersException(String s){
		super(s);
	}
	
	public BookInvalidPageNumbersException(Throwable t){
		super(t);
	}
	
	public BookInvalidPageNumbersException(String s, Throwable t){
		super(s, t);
	}

}
