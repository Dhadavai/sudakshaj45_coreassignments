//Printing only Odd numbers from the given array.
package coreassignments;

public class PrintOnlyOddNosFromArray2 {
	public static void main(String[] args) {
		int number[] = {12, 36, 87, 49, 62, 9, 18, 67};
		for( int i = 0; i < 8; i++){
			if(number[i]%2!=0){
				System.out.print(number[i] + " ");
			}
		}

	}

}
