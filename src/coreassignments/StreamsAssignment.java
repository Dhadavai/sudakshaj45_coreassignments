package coreassignments;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Objects;
import java.util.regex.Pattern;

public class StreamsAssignment {
	public static void main(String[] args) {
		FileInputStream in = null;
		String data = new String();
		int lowerCaseCount = 0;
		int upperCaseCount = 0;
		int specialCharsCount = 0;
		int noOfLines = 1;
		int digitsCount = 0;
		try {
			in = new FileInputStream("D:\\Sudaksha\\TestData\\Sample.txt");
			// System.out.println((char)in.read());
			int i;
			while ((i = in.read()) != -1) {
				System.out.print((char) i);
				data = data + ((char) i);
				char singleChar = (char) i;
				String singleString = Character.toString(singleChar);
				if (Pattern.matches("[A-Z]", singleString)) {
					upperCaseCount++;
				} else if (Pattern.matches("[a-z]", singleString)) {
					lowerCaseCount++;
				} else if (Pattern.matches("[0-9]", singleString)) {
					digitsCount++;
				} else if (Pattern.matches("\n", singleString)) {
					noOfLines++;
				} else {
				}
				specialCharsCount++;
			}

			System.out.println();

			String[] noOfWords = data.split(" ");
			System.out.println("#################################################");
			System.out.println("No of Characters in a file are: " + data.length());
			System.out.println("No of Words in a file are: " + noOfWords.length);
			System.out.println("No of Uppercase letters in a file are: " + upperCaseCount);
			System.out.println("No of Lowercase letters in a file are: " + lowerCaseCount);
			System.out.println("No of Special characters in a file are: " + specialCharsCount);
			System.out.println("No of digits in a file are: " + digitsCount);
			System.out.println("No of lines in a file are: " + noOfLines);

		} catch (FileNotFoundException fnf) {

		} catch (IOException ioe) {

		} finally {
			// if (in != null) {
			if (Objects.nonNull(in)) {

				try {
					in.close();
				} catch (IOException ioe) {

				}
			}

		}

	}

}
