// Printing second max number from an array.
package coreassignments;

public class FindSeconMaxFromArray2 {
	public static void main(String[] args) {
		int a[] = {12, 36, 93, 49, 62, 9, 18, 67, 99};
		int b[] = new int[9];
		int max = a[0];
		int secondMax = b[0];
		for(int i = 1; i < 9; i++){
			if(max < a[i]){
			max = a[i];
			}
		}

		for(int i = 0; i < 9; i++){
			if(a[i]!=max){
				b[i]=a[i];
			}
		}
		for(int i =1; i <=8; i++){
			if(secondMax < b[i]){
				secondMax = b[i];
			}
		}
		System.out.println("Second Max is: " + secondMax);
	}

}
