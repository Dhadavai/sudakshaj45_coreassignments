// Print maximum value from the array
package coreassignments;
public class MaxNoFromArray2 {

	public static void main(String[] args) {
		
		int number[] = {12, 36, 87, 49, 62, 9, 18, 67};
		int maxNumber = number[0];
		for(int i = 1; i < 8; i++){
		if(maxNumber < number[i]){
			maxNumber = number[i];
		}
	}
	System.out.println("Maximum number is " + maxNumber);
	}
	

}
