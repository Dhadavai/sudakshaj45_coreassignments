// Finding minimum number from an Array
package coreassignments;

public class MinNoFromArray2 {
	public static void main(String[] args) {
		int[] number = {45, 73, 43, 6, 98, 34, 11};
		int minNumber = number[0];
		for(int i = 1; i < 7; i++){
			if(minNumber > number[i]){
				minNumber = number[i];
			}
		}
		System.out.println("The minimum number from Array is: " + minNumber);
	}

}
