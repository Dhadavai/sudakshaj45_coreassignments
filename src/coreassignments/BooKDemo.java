package coreassignments;

public class BooKDemo {

	public static void main(String[] args) {
		Book book1 = new Book();
		System.out.println(book1.getTitle());
		System.out.println(book1.getPublisher());
		System.out.println(book1.getNoOfPages());
		System.out.println(book1.getPrice());
		System.out.println(book1.getCurrentPage());
		
		Book book2 = new Book("ITC Publisher");
		System.out.println(book2.getTitle());
		System.out.println(book2.getPublisher());
		System.out.println(book2.getNoOfPages());
		System.out.println(book2.getPrice());
		System.out.println(book2.getCurrentPage());
		
		Book book3 = new Book("Anu's", "Learn Java", 295.50, 750);
		System.out.println(book3.getTitle());
		System.out.println(book3.getPublisher());
		System.out.println(book3.getNoOfPages());
		System.out.println(book3.getPrice());
		System.out.println(book3.getCurrentPage());
		
		Book book4 = new Book();
		book4.setTitle("Java Book");
		System.out.println(book4.getTitle());
		
		book4.setNoOfPages(600);
		System.out.println(book4.getNoOfPages());
		
		book4.setPrice(299.99);
		System.out.println(book4.getPrice());
		
		book4.setCurrentPage(6);
		System.out.println(book3.getCurrentPage());
		
		book3.open();
		book4.open(595);
		System.out.println(book4.getCurrentPage()); 
		book4.closeBook();
		System.out.println(book4.getCurrentPage());
		
		book4.turnLeft(6);
		System.out.println(book4.getCurrentPage());
		book4.turnRight(15);
		System.out.println(book4.getCurrentPage());
		book4.turnLeft();
		System.out.println(book4.getCurrentPage());
		book4.turnRight();
		System.out.println(book4.getCurrentPage());
		
		Book book5 = new Book();
		System.out.println("No of Pages: " + book5.getNoOfPages());
		System.out.println("Price : " + book5.getPrice());
		System.out.println("Page: " + book5.getCurrentPage());
		try {
			book5.setCurrentPage(-200);
		}catch(BookInvalidPageNumbersException bipne){
			System.out.println("You have entered ivalid page number. Please correct it!");
		}
		try{
			book5.setPrice(-900);
		}catch(BookInvalidPriceException bipe){
			System.out.println("You have entered invalid price. Please enter correct price!");
		}
		try{
			book5.setNoOfPages(-600);
		}catch(BookInvalidPageNumbersException bipne){
			System.out.println("You have entered invalid page numbers. Please correct it!");
		}
		System.out.println("No of Pages: " + book5.getNoOfPages());
		System.out.println("Price : " + book5.getPrice());
		System.out.println("Page: " + book5.getCurrentPage());
 
	}

}
