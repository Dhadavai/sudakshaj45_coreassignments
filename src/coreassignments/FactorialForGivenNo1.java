package coreassignments;

public class FactorialForGivenNo1 {
	public static void main(String[] args) {
		int givenNumber =5;
		int fact=1;
		for(int i = 1; i <= givenNumber; i++){
			fact = fact * i;
		}
		System.out.println("Factorial of " + givenNumber + " is: " + fact);

	}

}
