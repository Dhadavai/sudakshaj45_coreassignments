//Printing only the numbers which are divisible by a given number
package coreassignments;

public class PrintDivisibleByGivenNumberFromArray2 {
	public static void main(String[] args) {

		int number[] = {12, 36, 87, 49, 62, 9, 18, 67};
		int divisibleByNo = 18;
		for(int i = 0; i < 8; i++){
			if(number[i]%divisibleByNo==0){
				System.out.print(number[i] + " ");
			}
		}

	}

}
