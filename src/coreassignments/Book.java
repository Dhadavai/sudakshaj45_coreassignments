package coreassignments;

public class Book {
	private int noOfPages;
	private double price;
	private int currentPage;
	private String title;
	private final String PUBLISHER;
	
	public Book(){
		
		this("Sri's");

	}
	public Book(String publisher){

		this.PUBLISHER = publisher;
		noOfPages = 600;
		price = 99.99;
		currentPage = 1;
		title = "Java";
	}
	
	public Book(String publisher, String title, double price, int noOfPages){
		this.PUBLISHER = publisher;
		this.title = title;
		if(price > 0){
			this.price = price;
		}else {
			throw new BookInvalidPriceException("for Price: " + price);
		}

		if(noOfPages > 0){
			this.noOfPages = noOfPages;
		} else {
			throw new BookInvalidPageNumbersException("for No Of Pages" + noOfPages);
		}
	}
	
	public int getNoOfPages(){
		return noOfPages;
	}
	
	public double getPrice(){
		return price;
	}
	
	public int getCurrentPage(){
		return currentPage;
	}
	
	public String getTitle(){
		return title;
	}
	
	public String getPublisher(){
		return PUBLISHER;
	}
	
	public void setNoOfPages(int noOfPages){
		if(noOfPages >= 0){
			this.noOfPages = noOfPages;
		}
		else {
			//System.out.println("Invalid page numbers, Please Enter valid page numbers");
			BookInvalidPageNumbersException bipe = new BookInvalidPageNumbersException("for no of pages: " + noOfPages);
			
			throw bipe;
			
		}
	}
	public void setPrice(double price){
		if(price >= 0){
			this.price = price;
		}
		else {
			//System.out.println("Invalid Price, Plesae enter valid Price.");
			throw new BookInvalidPriceException("for Price: " + price);
		}
	}
	
	public void setCurrentPage(int currentPage){
		if((currentPage > 0) && (currentPage <= noOfPages)){
			this.currentPage = currentPage;
		}
		else {
			//System.out.println(currentPage + " page number is not present in the book. Book has only " + noOfPages + " pages.");
			throw new BookInvalidPageNumbersException("For Current Page: " + currentPage);
		}
	}
	
	public void setTitle(String title){
		this.title = title;
	}
	
/*	public void setPublisher(String publisher){
		this.publisher = publisher ;
	}*/
	
	public void open(){
		System.out.println("You are in page number: " + currentPage);
	}
	public void open(int pageNo){
		if((pageNo > 0) && (pageNo <= noOfPages)) {
			currentPage = pageNo;
			System.out.println("You are in page: " + currentPage);
		}
		else {
			System.out.println(pageNo + " page number is not present in the book. Book has only " + noOfPages + " pages.");
		}
	}
	
	public void closeBook(){
		if(currentPage ==noOfPages){
			currentPage = 0;
			System.out.println("You have completed readig this book. Thank you for reading this book!!!");
		}
		else{
			System.out.println("Take a break, comeback again for Reading!!!");
		}
	}
	
	public void turnLeft(int pages){
		if(currentPage == noOfPages){
			System.out.println("Already you are in last page and do not have pages to turn left");
		}
		else {
			currentPage = currentPage + pages;
			if(currentPage >= noOfPages){
				currentPage = noOfPages;
			}
			System.out.println("You are in page number: " + currentPage);
		}
	}
	
	public void turnRight(int pages) {
		if(currentPage == 0 ){
			System.out.println("You are in first page and do not have pages to turn right");
		}
		else {
			currentPage = currentPage - pages;
			if(currentPage < 0){
				currentPage = 0;
			}
			System.out.println("You are in page number: " + currentPage);
		}
	}
	
	public void turnRight(){
		if(currentPage == 0){
			System.out.println("You are in first page and do not have pages to turn right");
		}
		else {
			currentPage = currentPage - 1;
		}
		
	}
	
	public void turnLeft(){
		if(currentPage == noOfPages){
			System.out.println("Already you are in last page and do not have pages to turn left");
		}
		else {
			currentPage = currentPage + 1;
			if(currentPage >= noOfPages){
				currentPage = noOfPages;
			}
			System.out.println("You are in page number: " + currentPage);
		}
	}
	

}
