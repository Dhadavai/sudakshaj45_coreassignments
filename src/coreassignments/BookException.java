package coreassignments;

public class BookException extends RuntimeException {
	
	public BookException(){
		super();
	}
	
	public BookException(String s){
		super(s);
	}
	
	public BookException(Throwable t){
		super(t);
	}
	
	public BookException(String s, Throwable t){
		super(s, t);
	}

}
