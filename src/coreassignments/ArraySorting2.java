package coreassignments;

public class ArraySorting2 {

	public static void main(String[] args) {

		int[] iArr1 = {55, 69, 36, 45, 98, 78, 3};
		int temp =0;
		//Bubble Sort
		for(int i = 0; i < iArr1.length-1; i++){
			for(int j = 0; j < iArr1.length-1-i; j++){

				if(iArr1[j]>iArr1[j+1]){
					temp = iArr1[j+1];
					iArr1[j+1] = iArr1[j];
					iArr1[j] = temp;
				}
			}
		}
		System.out.println("=============Result of Bubble Sort =========");
		for(int i = 0; i < iArr1.length; i++){
			System.out.print(iArr1[i] + " ");
		}
		System.out.println();
		
		// Selection Sort
		
		int[] iArr2 = {23, 239, 101, 45, 98, 783, 333};
		
		for(int i = 0; i < iArr2.length-1; i++){
			int minIndex = i;
			for(int j = i+1 ; j < iArr2.length; j++){
				
			
				if(iArr2[j] < iArr2[minIndex]){
					minIndex = j;
				}
			}
			temp = iArr2[minIndex];
			iArr2[minIndex] = iArr2[i];
			iArr2[i] = temp;
					
		}
		
		System.out.println("=============Result of Selection Sort =========");
		for(int j = 0; j < iArr2.length; j++){
			System.out.print(iArr2[j] + " ");
		}

	}

}
