package coreassignments;

public class BookInvalidPriceException extends BookException {
	
	public BookInvalidPriceException(){
		super();
	}
	
	public BookInvalidPriceException(String s){
		super(s);
	}
	
	public BookInvalidPriceException(Throwable t){
		super(t);
	}
	
	public BookInvalidPriceException(String s, Throwable t){
		super(s, t);
	}

}
