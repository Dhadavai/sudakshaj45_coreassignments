package coreassignments;

public class CourseDemo {

		
	public static void main(String[] args) {
		System.out.println(Course.INSTITUTE);
		Course course1 = new Course("java");
		course1.getCourseDetails();
		course1.getStudentDetails();
		course1.setStudentName("Srinvas");
		course1.setStudentCourse("Java");
		course1.setStudentRollNumber("J001");
		course1.getStudentDetails();
		
		Course.INSTITUTE = "ANUTECH";
		
		course1.getCourseDetails();
		course1.getStudentDetails();
		
		Course course2 = new Course("Python");
		course2.setStudentName("Anu");
		course2.setStudentCourse("Python");
		course2.setStudentRollNumber("P001");
		
		course2.getCourseDetails();
		course2.getStudentDetails();
		
		
		
	}

}
