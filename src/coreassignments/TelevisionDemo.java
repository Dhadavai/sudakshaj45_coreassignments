package coreassignments;

import com.guruofjava.sudaksha.j45.LGTV;

public class TelevisionDemo {

	public static void main(String[] args) {
		
		//Television t1 = new Television();
		//System.out.println(t1.BRAND_NAME);
		//System.out.println(t1.SIZE);
		//System.out.println(t1.price);
		//t1.SIZE=15; // SIZE is final we cannot assign value
		//System.out.println(t1.SIZE);
		System.out.println("================================");
		
		//Television t2 = new Television();
		//System.out.println(t2.BRAND_NAME);
		//System.out.println(t2.SIZE);
		//System.out.println(t2.price);
		System.out.println("================================");
		
		Television t3 = new Television("Panasonic");
		System.out.println(t3.BRAND_NAME);
		System.out.println(t3.SIZE);
		//System.out.println(t3.price);
		System.out.println("================================");
		
		Television t4 = new Television("LG", 32.25);
		System.out.println(t4.BRAND_NAME);
		System.out.println(t4.SIZE);
		//System.out.println(t4.price);
		System.out.println("================================");
		
		Television t5 = new Television("SAMSUNG", 56.59, 56000.99);
		System.out.println(t5.BRAND_NAME);
		System.out.println(t5.SIZE);
		//System.out.println(t5.price);
		System.out.println("================================");
		
		System.out.println("Brightness: " + t5.getCurrentBrightness());
		System.out.println("Channel: " + t5.getCurrentChannel());
		System.out.println("Volume: " + t5.getCurrentVolume());
		t5.increaseBrightness();
		t5.increaseBrightness();
		t5.increaseBrightness();
		System.out.println("Brightness: " + t5.getCurrentBrightness());
		t5.decreaseBrightness();
		System.out.println("Brightness: " + t5.getCurrentBrightness());
		t5.nextChannel();
		t5.nextChannel();
		t5.nextChannel();
		System.out.println("Channel: " + t5.getCurrentChannel());
		t5.previousChannel();
		System.out.println("Channel: " + t5.getCurrentChannel());
		t5.increaseVolume();
		t5.increaseVolume();
		t5.increaseVolume();
		t5.increaseVolume();
		System.out.println("Volume: " + t5.getCurrentVolume());
		t5.decreaseVolume();
		System.out.println("Volume: " + t5.getCurrentVolume());
		t5.setChannel(786);
		System.out.println("Channel: " + t5.getCurrentChannel());
		t5.nextChannel();
		System.out.println("Channel: " + t5.getCurrentChannel());
		t5.powerOnOff();
		System.out.println(t5.getState());
		t5.powerOnOff();
		System.out.println(t5.getState());
		t5.powerOnOff();
		System.out.println(t5.getState());
		System.out.println(t5.getPrice());
		t5.setPrice(-2000);
		System.out.println(t5.getPrice());
		
		SmartTV s1 = new SmartTV();
		System.out.println(s1.getPrice());

		SmartTV s2 = new SmartTV("Android");
		System.out.println(s2.getCurrentChannel());
		
		LGTV t6 = new LGTV();
		System.out.println(t6.getCurrentBrightness());
		System.out.println(t6.getCurrentVolume());
		System.out.println(t6.getCurrentChannel());
		

	}

}
