package coreassignments;

public class Course {
	
	private String courseName;
	private double fee;
	private String faculty;
	private String studentName;
	private String studentRollNumber;
	private String studentCourse;
	static  String INSTITUTE = "Sudaksha";
	
	
	public Course(String courseName){
		
		if(courseName.equalsIgnoreCase("Java")){
			this.courseName = courseName;
			fee = 29500.00;
			faculty = "Rupesh";
		}
		else if(courseName.equalsIgnoreCase("Selenium")){
			this.courseName = courseName;
			fee = 15000.00;
			faculty = "Srinvas";
		}
		else if (courseName.equalsIgnoreCase("Python")){
			this.courseName = courseName;
			fee = 59000.00;
			faculty = "Naveen";
		}
				
	}
	
	public void setStudentName(String studentName){
		this.studentName = studentName;
	}
	
	/*public String getStudentName(){
		return studentName;
	}*/
	
	public void setStudentCourse(String studentCourse){
		this.studentCourse = studentCourse;
	}
/*	public String getStudentCourse(){
		return studentCourse;
	}*/
	
	public void setStudentRollNumber(String studentRollNumber){
		this.studentRollNumber = studentRollNumber;
	}
	/*public String getStudentRollNumber(){
		return studentRollNumber;
	}*/
	
	public void getCourseDetails(){
		System.out.println("----Course Details----");
		System.out.println("Name: " + courseName);
		System.out.println("Fee: " + fee);
		System.out.println("Faculty: " + faculty);
		System.out.println("Institute: " + INSTITUTE);	
	}
	
	public void getStudentDetails(){
		System.out.println("Student Name: " + studentName);
		System.out.println("Student Course: " + studentCourse);
		System.out.println("Student Roll Number: " + studentRollNumber);
	}
}
